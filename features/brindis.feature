#language: es
@wip
Característica: Presupuestación de casamiento

  Antecedentes:
    Dado que mi cuit personal es "20445556667"
    Y es el día "2021-04-05" que no es jueves

  Escenario: b1 - Casamiento de más de 50 comensales con brindis
    Dado un evento "casamiento" con servicio "normal"
    Y se contrata el brindis
    Y que está programado para el "2021-05-03" que es un día hábil
    Y que tendrá 100 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 22100.0

  Escenario: b2 - Casamiento discreto con menos de 50 comensales con brindis
    Dado un evento "casamiento" con servicio "normal"
    Y se contrata el brindis
    Y que está programado para el "2021-05-03" que es un día hábil
    Y que tendrá 10 comensales con menú "mixto"
    Cuando se presupuesta
    Entonces el importe resultante es 2350.0

  Escenario: b3 - Fiesta de 15 no puede incluir brindis
    Dado un evento "fiesta de 15" con servicio "normal"
    Y se contrata el brindis
    Cuando intento programarlo para el "2021-05-11"
    Entonces obtengo un error

  Escenario: b4 - Fiesta de graduación no puede incluir brindis
    Dado un evento "fiesta de graduacion" con servicio "normal"
    Y se contrata el brindis
    Cuando intento programarlo para el "2021-05-11"
    Entonces obtengo un error

  Escenario: b5 - Evento empresarial no puede incluir brindis
    Dado un evento "empresarial" con servicio "normal"
    Y se contrata el brindis
    Cuando intento programarlo para el "2021-05-11"
    Entonces obtengo un error
